import scss from "rollup-plugin-scss";
import typescript from '@rollup/plugin-typescript';
import bundleInject from "rollup-plugin-bundle-inject"
import del from 'rollup-plugin-delete'

export default {
    input: "src/index.ts",
    output: {
        file: "dist/bundle.js",
        format: 'cjs'
    },
    plugins: [
        del({targets: "dist/*"}),
        scss({
            name: "style.min.css",
            outputStyle: "compressed",
            sourceMap: true,
        }),
        typescript({compilerOptions: {lib: ["es5", "es6", "dom"], target: "es5"}}),
        bundleInject({
            target: "./scr/index.html",
            minify: true,
        })
    ]
}